# README #

Mailshare is a tool that I use to share files over email.

I wrote it for an use case of mine: I wanted to easily share files but I don't want to go through the various cloud services. With this tool I send the files I want to share as email attachments to a dedicated email address, and I will get back a link to these files on my personal web site.

**WARNING: DO NOT RUN ON YOUR PERSONAL EMAIL, IT WILL DELETE ALL YOUR NEW MESSAGES**

Also, be aware that anyone that knows the sharing email will be able to upload files. I will implement OTP in the future to avoid this.

## Requirements ##

* [appdirs](https://pypi.python.org/pypi/appdirs/1.4.0), a library to get local application directories in a portable manner.
* an IMAP4-enabled email address to use for sharing. It is recommended to use a dedicated email, since the script will only check for new emails.
* a web server with write access to a dedicated sharing directory where you can write to.

## Installation ##

* Install the dependency, appdirs: 

```bash
pip install appdirs # run as root, or as user with --user
```

The local installation (with --user) will fail on OSX. The workaround is the following:

```bash
pip install appdirs --user --prefix=
```

* Install ```mailshare```:

```bash
git clone https://git@bitbucket.org/insomniacslk/mailshare.git
cd mailshare
python setup.py install # run as root, or as user with --user
```

## Setup ##

You need:
* a dedicated email address. Don't use your own email
* a web server with write access to a directory under the document root
* a configuration file for ```mailshare```
* access to a cron tab to run the script that will copy the attachments to the appropriate place

In detail:

* email address: you need an IMAP4-enabled email address. Take note of the server's IMAP4 host name, the username and the password to fetch the email, and optionally the server port if different from the default.

* web server: you need to be able to write to a web server's subdirectory that will be used for sharing purposes. Take note of this, and set the appropriate read and write permissions.

* Configuration file: run ```python -m mailshare``` for the first time to get a sample
configuration file. E.g.:

```bash
$ python -m mailshare
The configuration file does not exist.
Create the file /Users/insomniac/Library/Application Support/mailshare/configuration.json with the following content, adapt it to your needs
and re-run mailshare.

{
    "email": {
        "server": "YOUR_MAIL_SERVER",
        "username": "YOUR_MAILSHARE_EMAIL_OR_USERNAME",
        "password": "YOUR_MAILSHARE_PASSWORD",
        "transport": "ssl", # or "tls" or "plaintext"
        "sender": 'someone@example.com',
        "notify_to": ["someoneelse@example.com"]
    },
    "www": {
        "document_root": "WHERE_TO_SAVE_YOUR_FILES",
        "folder_pattern": "share/%Y-%m-%d", # parsed by datetime.strftime
        "base_url": "https://YOUR_WEBSITE_URL_WITH_PATH"
    }
}
```

* a crontab: once you have created the configuration file, you have to run ```python -m mailshare``` to check for mail. This is run only once, there is no daemon mode, therefore the recommended way is to use a cron job. The following, for example, will check mail every minute:

```bash
* * * * python -m mailshare
```

Set it up as the user that has write access to the webserver directory.
If you need to check more frequently, either wait for me to add this feature (or make a pull request), or use something like [this advice](http://stackoverflow.com/questions/1034243/how-to-get-a-unix-script-to-run-every-15-seconds) .

## TODO ##

* search for emails in an IMAP sub-folder
* search by pattern
* let the user to choose not to delete the email after fetching it
* implement the use of OTP, to avoid that anyone can upload files if they know the sharing email address
* logging
* testing

## Contact me ##

My name is Andrea Barberio. You can find my contacts on https://insomniac.slackware.it .
