import imaplib

from . import imaptls
from . import common

# TODO implement support for keyfile and certfile


class MailPoller(object):
    def __init__(self, server, username, password, port=None,
                 transport='tls',
                 imap_path=None):
        self.server = server
        self.username = username
        self.password = password
        self.port = port
        self.transport = transport
        self.imap_path = imap_path
        self.imap = None

    def __del__(self):
        self.close()

    def get_default_port(self):
        if self.transport in (common.PLAINTEXT, common.TLS):
            return imaplib.IMAP4_PORT
        elif self.transport == common.SSL:
            return imaplib.IMAP4_SSL_PORT
        else:
            raise ValueError('Invalid transport: {t!r}'.format(
                t=self.transport))

    def connect(self):
        if self.port is None:
            self.port = self.get_default_port()

        # select transport method
        if self.transport == common.PLAINTEXT:
            IMAPClass = imaplib.IMAP4
        elif self.transport == common.TLS:
            IMAPClass = imaptls.IMAP4
        elif self.transport == common.SSL:
            IMAPClass = imaplib.IMAP4_SSL
        else:
            raise ValueError('Invalid transport: {t!r}'.format(
                t=self.transport))

        # connect
        self.close()
        try:
            self.imap = IMAPClass(self.server, self.port)
        except imaplib.IMAP4.error:
            # TODO log the message
            raise

        # log in
        try:
            if self.transport == common.TLS:
                self.imap.starttls()
            self.imap.login(self.username, self.password)
            self.imap.select()
        except imaplib.IMAP4.error:
            # TODO log the message
            raise

    def fetch_new_messages(self):
        # check for new messages
        try:
            retcode, data = self.imap.search(None, '(UNSEEN)')
        except imaplib.IMAP4.error:
            # TODO log the message
            raise
        if retcode == 'OK':
            indexes = data[0]
            messages = []
            indexes = map(int, indexes.split())
            for index in indexes:
                messages.append(self.imap.fetch(index, '(RFC822)'))
            return messages

    def close(self):
        if self.imap is not None:
            self.imap.close()
            self.imap.logout()
            self.imap = None
