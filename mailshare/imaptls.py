""" Python IMAP with TLS/SSL support """
#
# Author: Alexander Brill <alex@brill.no>
# Copyright (C) 2004 Alexander Brill
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#

import imaplib
import socket

"""USAGE:
import imaptls
imap = imaptls.IMAP4('hostname')
imap.starttls(keyfile=None, certfile=None)

# create a simple function for our PLAIN login
def sendAuth(response):
    return "user\0auth\0password"

typ, data = imap.authenticate("PLAIN", sendAuth)
"""

# from https://gist.github.com/obeleh/7752132


class SSLFakeSocket:
    """A fake socket object that really wraps a SSLObject.
    It only supports what is needed in smtplib.
    """
    def __init__(self, realsock, sslobj):
        self.realsock = realsock
        self.sslobj = sslobj

    def send(self, str):
        self.sslobj.write(str)
        return len(str)

    sendall = send

    def close(self):
        self.realsock.close()

    def shutdown(self, *args, **kwargs):
        pass


class SSLFakeFile:
    """A fake file like object that really wraps a SSLObject.
    It only supports what is needed in smtplib.
    """
    def __init__(self, sslobj):
        self.sslobj = sslobj

    def readline(self):
        str = ""
        chr = None
        while chr != "\n":
            chr = self.sslobj.read(1)
            if not chr:
                break
            str += chr
        return str

    def read(self, size):
        tmp = ""
        prevLen = -1
        tmpLen = 0
        while tmpLen != prevLen and tmpLen != size:
            prevLen = tmpLen
            tmp += self.sslobj.read(size - prevLen)
            tmpLen = len(tmp)
        return tmp

    def close(self):
        pass

Commands = {
    'STARTTLS': ('NONAUTH')
    }
imaplib.Commands.update(Commands)


class IMAP4(imaplib.IMAP4):

    def starttls(self, keyfile=None, certfile=None):
        name = "STARTTLS"
        typ, dat = self._simple_command(name)
        if typ != 'OK':
            raise self.error(dat[-1])
        sslobj = socket.ssl(self.sock, keyfile, certfile)
        self.sock = SSLFakeSocket(self.sock, sslobj)
        self.file = SSLFakeFile(sslobj)

        cap = 'CAPABILITY'
        self._simple_command(cap)
        if cap not in self.untagged_responses:
            raise self.error('no CAPABILITY response from server')
        self.capabilities = tuple(
            self.untagged_responses[cap][-1].upper().split()
        )
