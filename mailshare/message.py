import os
import copy
import email


DEFAULT_EXCLUDED_CONTENT_TYPES = ['text/html', 'text/plain']


class Message(object):

    def __init__(self, message):
        self.message = message
        self.mail = email.message_from_string(self.message)

    def get_attachments(self, exclude_content_types=None):
        '''
        Extract all the attachments from the message. By default plaintext
        attachments are excluded, but you can set your own filter with the list
        `exclude_content_types'

        The return value is a list of (content-type, file name, file content)
        '''
        if not self.mail.is_multipart():
            return []

        attachments = []
        if exclude_content_types is None:
            excluded_content_types = DEFAULT_EXCLUDED_CONTENT_TYPES
        for item in self.mail.get_payload():
            content_type = item.get_content_type()
            if content_type in excluded_content_types:
                continue
            filename = item.get_filename()
            if filename:
                attachments.append((content_type, filename,
                                    item.get_payload(decode=True)))
        return attachments

    def save_attachments(self, output_directory, excluded_content_types=None):
        '''
        Save the attachments to the specified output directory. The content-type
        exclusion works as in `get_attachments'.
        '''
        if not os.path.exists(output_directory):
            try:
                os.makedirs(output_directory)
            except OSError:
                # TODO log the exception
                raise

        saved_files = []
        attachments = self.get_attachments(excluded_content_types)
        for content_type, file_name, file_content in attachments:
            try:
                # TODO check for duplicate file names in the attachments
                outfile = os.path.join(output_directory, file_name)
                with open(outfile, 'wb') as fd:
                    fd.write(file_content)
                saved_files.append(file_name)
            except (OSError, IOError):
                # TODO log the exception
                raise
        return saved_files

    def without_attachments(self):
        '''
        Return a copy of the message without the attachments
        '''
        newmsg = copy.copy(self.mail)
        if not newmsg.is_multipart():
            return newmsg

        replacement_template = 'Attachment removed: {fn}'

        def strip_attachments(part):
            filename = part.get_filename()
            if filename:
                replacement = replacement_template.format(fn=filename)
                part.set_payload(replacement)
                del part['Content-Transfer-Encoding']
                del part['Content-Disposition']
                part.set_type('text/plain')
            else:
                if part.is_multipart():
                    payload = [strip_attachments(p) for p in part.get_payload()]
                    part.set_payload(payload)
            return part

        return strip_attachments(newmsg)
