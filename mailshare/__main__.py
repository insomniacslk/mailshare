import os
import sys
import json
import urllib
import datetime

import appdirs

import mailshare.mailpoller
import mailshare.mailsender
import mailshare.common
import mailshare.message

APP_NAME = 'mailshare'
APP_AUTHOR = 'insomniac'


def get_configuration_help():
    conf_help = '''The configuration file does not exist.
Create the file {configfile} with the following content, adapt it to your needs
and re-run {app}.

{{
    "email": {{
        "server": "YOUR_MAIL_SERVER",
        "username": "YOUR_MAILSHARE_EMAIL_OR_USERNAME",
        "password": "YOUR_MAILSHARE_PASSWORD",
        "transport": "ssl", # or "tls" or "plaintext"
        "sender": 'someone@example.com',
        "notify_to": ["someoneelse@example.com"]
    }},
    "www": {{
        "document_root": "WHERE_TO_SAVE_YOUR_FILES",
        "folder_pattern": "share/%Y-%m-%d", # parsed by datetime.strftime
        "base_url": "https://YOUR_WEBSITE_URL_WITH_PATH"
    }}
}}
'''.format(configfile=get_configuration_file(), app=APP_NAME)
    return conf_help


def get_url(base_url, folder, filename):
    path = '{folder}/{filename}'.format(
        folder=urllib.quote(folder),
        filename=urllib.quote(filename))
    return '{base}/{path}'.format(
        base=base_url,
        path=path,
    )


def get_configuration_file():
    confdir = appdirs.user_data_dir(APP_NAME, APP_AUTHOR)
    return os.path.join(confdir, 'configuration.json')


def get_configuration():
    configuration_file = get_configuration_file()
    print("Config file: {}".format(configuration_file))
    try:
        configuration = json.load(open(configuration_file))
    except (OSError, IOError):
        print(get_configuration_help())
        sys.exit(1)
    return configuration


def get_folder(pattern):
    now = datetime.datetime.now()
    folder = datetime.datetime.strftime(now, pattern.lstrip('/'))
    return folder


def send_urls(config, urls, recipients, headers=None, attachments=None):
    if not urls:
        # nothing to send
        return
    ms = mailshare.mailsender.MailSender(
        config['email']['server'],
        config['email']['username'],
        config['email']['password'],
        transport=config['email']['transport'],
    )
    body = '''Please find below the requested links

{urls}

Glad to be of service,
MailShare
'''.format(urls='\n'.join('{url}'.format(url=url) for url in urls))
    mail_headers = {}
    if headers is not None:
        mail_headers.update(headers)

    notify_to = ', '.join(config['email']['notify_to'])
    try:
        mail_headers['Bcc'].append(notify_to)
    except KeyError:
        mail_headers['Bcc'] = notify_to
    ms.send(
        sender=config['email']['sender'],
        recipients=recipients,
        body=body,
        headers=mail_headers,
        attachments=attachments,
    )


def main():
    config = get_configuration()

    poller = mailshare.mailpoller.MailPoller(
        config['email']['server'],
        config['email']['username'],
        config['email']['password'],
        transport=config['email']['transport'],
    )
    poller.connect()
    messages = poller.fetch_new_messages()
    poller.close()

    if len(messages) == 0:
        print('No new messages')
        sys.exit(os.EX_OK)

    folder = get_folder(config['www']['folder_pattern'])
    outdir = os.path.join(config['www']['document_root'], folder)
    urls = []
    for message in messages:
        status, msgdata = message
        (criteria, text), flags = msgdata
        original_email = mailshare.message.Message(text)
        saved_files = original_email.save_attachments(outdir)
        subject = original_email.mail.get('Subject', '(no subject)')
        if not subject.startswith('Re:'):
            subject = 'Re: {s}'.format(s=subject)
        for filename in saved_files:
            urls.append(
                get_url(
                    config['www']['base_url'],
                    folder,
                    filename,
                )
            )
        recipients = [original_email.mail['From']]
        send_urls(config=config, urls=urls, recipients=recipients,
                  headers=(('Subject', subject),),
                  attachments=[original_email.without_attachments()])


if __name__ == '__main__':
    main()
