import socket
import smtplib
import email.mime.multipart
import email.mime.text

from . import common


class MailSender(object):
    def __init__(self, server, username, password, port=None,
                 transport=common.TLS,
                 local_hostname=None):
        self.server = server
        self.username = username
        self.password = password
        self.port = port
        self.transport = transport
        self.local_hostname = local_hostname
        self.smtp = None

    def get_default_port(self):
        if self.transport in (common.PLAINTEXT, common.TLS):
            return smtplib.SMTP_PORT
        elif self.transport == common.SSL:
            return smtplib.SMTP_SSL_PORT
        else:
            raise ValueError('Invalid transport: {t!r}'.format(
                t=self.transport))

    def connect(self):
        if self.port is None:
            self.port = self.get_default_port()

        # select transport method
        if self.transport in (common.PLAINTEXT, common.TLS):
            SMTPClass = smtplib.SMTP
        elif self.transport == common.SSL:
            SMTPClass = smtplib.SMTP_SSL
        else:
            raise ValueError('Invalid transport: {t!r}'.format(
                t=self.transport))

        # connect
        self.close()
        try:
            self.smtp = SMTPClass(self.server, self.port, self.local_hostname)
        except socket.timeout:
            # TODO log the message
            raise

        # identify ourselves
        self.smtp.ehlo_or_helo_if_needed()

        # use TLS if requested
        if self.transport == common.TLS:
            try:
                self.smtp.starttls()
            except (smtplib.SMTPHeloError, smtplib.SMTPException,
                    RuntimeError):
                # TODO log the message
                raise

        # log in
        try:
            self.smtp.login(self.username, self.password)
        except (smtplib.SMTPHeloError, smtplib.SMTPAuthenticationError,
                smtplib.SMTPException):
                # TODO log the message
                raise

    def send(self, sender, recipients, body, headers=None, attachments=None):
        '''
        Send the message.

        sender is a string suitable for the From header
        recipients is a list of strings suitable for the To header
        body is the text to send as a string
        headers is a dictionary or a tuple of headers and their values
        attachments is a list of objects to attach to the email
        '''
        msg = email.mime.multipart.MIMEMultipart()
        msg['Subject'] = '(No Subject)'
        msg['From'] = sender

        # Update the headers replacing them if necessary
        if headers is not None:
            for header, value in dict(headers).iteritems():
                try:
                    del msg[header]
                except KeyError:
                    pass
                msg[header] = value

        text = email.mime.text.MIMEText(body)
        msg.attach(text)
        if attachments is not None:
            for attachment in attachments:
                msg.attach(attachment)
        self.connect()
        self.smtp.sendmail(sender, recipients, msg.as_string())

    def close(self):
        '''
        Close the connection to the SMTP server if necessary
        '''
        if self.smtp is not None:
            self.smtp.quit()
            self.smtp = None
