from distutils.core import setup

setup(
    name='mailshare',
    version='1.0',
    author='Andrea Barberio',
    author_email='insomniac@slackware.it',
    description='Tool to merge and analyze OpenStack logs',
    url='https://bitbucket.org/insomniacslk/mailshare',
    packages=['mailshare'],
)
